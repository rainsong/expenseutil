var dateFormat = '1';
var expenseItems = new Array();
var lastCellAddress = 'A1';

function getVersion() {
    return chrome.app.getDetails().version;
}

function handleFile(e) {
    dateFormat = document.getElementById('date-format').value;

    var files = e.target.files;
    if (files.length != 1)
        return;

    var f = files[0];
    var reader = new FileReader();

    reader.onload = function (e) {
        try {
            var data = e.target.result;
            var workbook = XLSX.read(data, {
                type: 'binary'
            });

            var ws = workbook.Sheets['Template'];

            var rowIndex = 0;
            expenseItems = new Array();

            if (ws['A1'].v != 'Expense Template v' + getVersion())
                throw 'Wrong expense template file version';

            while (true) {
                var row = 4 + rowIndex;
                var expenseDate = ws['B' + row];
                if (expenseDate === '' || expenseDate === undefined || expenseDate === null)
                    break;

                var expenseItem = new Object();
                expenseItem.expenseDate = expenseDate.w;
                expenseItem.expenseType = ws[lastCellAddress = 'D' + row].v;
                if (ws[lastCellAddress = 'E' + row])
                    expenseItem.description = ws['E' + row].v;
                else
                    expenseItem.description = '';
                if (ws[lastCellAddress = 'F' + row])
                    expenseItem.amount = ws['F' + row].v;
                else
                    expenseItem.amount = '';
                expenseItem.currency = ws[lastCellAddress = 'G' + row].v;
                expenseItem.paymentType = ws[lastCellAddress = 'H' + row].v;
                expenseItem.billingType = ws[lastCellAddress = 'I' + row].v;

                cell = ws[lastCellAddress = 'J' + row];
                if (cell != undefined && cell.v != '')
                    expenseItem.mileage = cell.v;

                cell = ws[lastCellAddress = 'K' + row];
                if (cell != undefined && cell.v != '')
                    expenseItem.numberOfNight = cell.v;

                var attendees = new Array();
                while (true) {
                    var cellAddress = {
                        c: 11 + attendees.length * 3,
                        r: row - 1
                    };

                    if (!ws[XLSX.utils.encode_cell(cellAddress)])
                        break;

                    var attendee = new Object();
                    attendee.name = ws[lastCellAddress = XLSX.utils.encode_cell(cellAddress)].v;

                    cellAddress.c += 1;
                    attendee.company = ws[lastCellAddress = XLSX.utils.encode_cell(cellAddress)].v;

                    cellAddress.c += 1;
                    cell = ws[lastCellAddress = XLSX.utils.encode_cell(cellAddress)];
                    if (cell != undefined)
                        attendee.title = ws[lastCellAddress = XLSX.utils.encode_cell(cellAddress)].v;
                    else
                        attendee.title = '';

                    attendees.push(attendee);
                }
                expenseItem.attendees = attendees;

                expenseItems.push(expenseItem);
                rowIndex++;
            }

            console.log(expenseItems);
            start();
        } catch (error) {
            console.log('Error:' + error);
            console.log('LastCellAddress: ' + lastCellAddress);
            alert(
                'Excel file parsing error\n\n' +
                'Cell : ' + lastCellAddress + '\n\n' +
                error
            );
        }
    };

    reader.readAsBinaryString(f);
}

function start() {
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {
            action: 'popup_start_input',
            dateFormat: dateFormat,
            data: expenseItems
        });

        window.close();
    });
}

function displayReleaseNotes() {
    $.get('https://bitbucket.org/rainsong/expenseutil/wiki/Home', function (wikipage) {
        var wikicontent = $(wikipage).find('#wiki-content');
        $(wikicontent).find('a').attr('target', '_blank');
        $('#eu-release-notes').append($('<span/>', {
            html: wikicontent.html()
        }));
    });
}

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('eu-title').innerHTML = 'Expense Util v' + getVersion();
    document.getElementById('excel-file-selection').addEventListener('change', handleFile, false);
    displayReleaseNotes()
});

(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-64086978-2', 'auto');
ga('set', 'checkProtocolTask', function () {});
ga('require', 'displayfeatures');
ga('send', 'pageview', '/popup.html', {
    'dimension1': getVersion()
});
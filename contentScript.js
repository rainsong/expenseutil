var s = document.createElement('script');
s.src = chrome.extension.getURL('pageScript.js');

(document.head || document.documentElement).appendChild(s);

chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
    if (message.action === 'popup_start_input') {
        console.log(message.dateFormat);
        console.log(message.data);

        let event = new CustomEvent('content_start_input', {
            detail: message
        });
        window.dispatchEvent(event);
    }
});

window.addEventListener('message', function (event) {
    if (event.data.action === 'page_done')
        chrome.runtime.sendMessage(event.data);
}, false);